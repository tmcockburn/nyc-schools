//
//  _0210413_TylerCockburn_NYCSchoolsTests.swift
//  20210413-TylerCockburn-NYCSchoolsTests

//  Copyright © 2021 Tyler. All rights reserved.
//
//Quick example test case for the input csv value for the function in SchoolViewController.
import XCTest
@testable import _0210413_TylerCockburn_NYCSchools

class _0210413_TylerCockburn_NYCSchoolsTests: XCTestCase {
    
    var csvValidation: SchoolViewController!
    
    override func setUp() {
        super.setUp()
        csvValidation = SchoolViewController()
        
    }
    
    override func tearDown() {
        csvValidation = nil
        super.tearDown()
    }
    
    func is_valid_csvData() throws {
        let testPath = Bundle.main.path(forResource: "2012_SAT_Resultz", ofType: "csv")
        XCTAssertNoThrow(try csvValidation.captureData(filePath: testPath!))
        
        
    }
    
}
