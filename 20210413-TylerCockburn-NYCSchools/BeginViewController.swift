//
//  ViewController.swift
//  20210413-TylerCockburn-NYCSchools
//
//  Created by Harold on 4/13/21.
//  Copyright © 2021 Tyler. All rights reserved.
//

import UIKit

class BeginViewController: UIViewController {

    @IBAction func toSchoolList(_ sender: Any) {
        performSegue(withIdentifier: "toSchoolList", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

