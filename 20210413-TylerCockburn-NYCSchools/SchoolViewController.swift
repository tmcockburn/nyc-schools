//
//  SchoolViewController.swift
//  20210413-TylerCockburn-NYCSchools
//
//  
//  Copyright © 2021 Tyler. All rights reserved.
//

import UIKit

class SchoolViewController: UIViewController {
    //label only to be used as an anchor and formatting
    @IBOutlet weak var schoolListLabel: UILabel!
    //String variable to hold the name of the school selected
    var selectedSchool = String()
    //parameter for formatting programmatically
    var yPos:CGFloat = 0
    //dictionary that contains each school as the key, and the value is a struct containing the values from the table. More can be added here if there were more columns in the table
    var schoolInfo = [String:SchoolScores]()
    struct SchoolScores{
        //optional ints in the case that there are values entered that aren't integers
        var numTestTakers:Int?
        var readingScore:Int?
        var mathScore:Int?
        var writingScore:Int?
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //parse csv file included in the workspace. With more time, I would preferred using a database such as sqlite as two tables were given. Both tables could be inserted into the database and queried here. Writing code this would likely be much simpler, as you would just need to query the column of schools and the items for that school when one is selected.
        let csvPath = Bundle.main.path(forResource: "2012_SAT_Results", ofType: "csv")
        //populates dictionary
        captureData(filePath: csvPath!)
        
        //Now that we have a dictionary with all of the values stored, create a scrollview that has each school as a button that will display an alert message that contains the school's scores and any other data captured. My initial way of thinking of this was to segue all of the necessary data into another view controller. This would include the dictionary loaded above; that way I wouldn't have to repeat code and recreate it in the next controller. I would do this using the prepare() method. However, since there is no other functionality required for the question and all the necessary data is here, it made more sense to just display the scores on the same controller.
        
        //left the design and constraint code here and moved the app functionality to other functions
        var scrollView: UIScrollView = {
            let view = UIScrollView()
            //formatting for the scrollview
            view.translatesAutoresizingMaskIntoConstraints = false
            view.contentSize.height = 100
            view.backgroundColor = UIColor.clear
            return view
        }()
        //add the subview into the controller view
        view.addSubview(scrollView)
        
        //setting constraints so the scrollview is centered/formatted on all screen types
        scrollView.topAnchor.constraint(equalTo: schoolListLabel.bottomAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        //Items in the dictionary are not sorted. Since we only need the keys to be displayed from the dictionary for this screen, we can just sort them
        let sortedKeys = Array(schoolInfo.keys).sorted()
        //for each (sorted) key, create a button, format it, and add it to the scrollview
        for key in sortedKeys{
            var button = UIButton(frame: CGRect(x:100, y:100, width:300, height:100))
            button.setTitle(key, for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            button.titleLabel?.numberOfLines = 0
            button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            //creating a hint so there will be an accessible reference where needed
            button.accessibilityHint = key
            //creates an action for the button pressed
            button.addTarget(self, action: #selector(schoolPressed), for: .touchUpInside)
            button.contentHorizontalAlignment = .left
            button.center.x = view.center.x
            //splits each button in the scrollview evenly. With more time, I would've liked to refactor the code so lines are separated depending on the screen size rather than hard coded values
            button.frame.origin.y = yPos
            //increases the size of the scrollview based on entries
            scrollView.contentSize.height += 100
            scrollView.addSubview(button)
            yPos+=100
            
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func schoolPressed(sender:UIButton){
        selectedSchool = sender.accessibilityHint!
        //this should store the struct from the value in schoolInfo
        let selectedSchoolScores = schoolInfo[selectedSchool]
        if selectedSchoolScores?.readingScore == nil || selectedSchoolScores?.writingScore == nil || selectedSchoolScores?.mathScore == nil || selectedSchoolScores?.numTestTakers == nil{
            //alert dialog that explains there are no valid scores recorded for this school
            let dialogMessage = UIAlertController(title: selectedSchool, message: "Unfortunately, there was an error loading details for this school.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            })
            dialogMessage.addAction(ok)
            self.present(dialogMessage, animated: true, completion: nil)
        }
        else{
            
            //alert dialog that displays the scores for the school selected. Because the values are in the struct are optional, they need to be forcibly unwrapped here. Because of this, if there is an incorrect value in the csv, an error would appear here. To avoid this, I changed the conditional statement to include || instead of &&. This is a workaround that will essentially not show any information for a school if one of the values are bugged. Ultimately, I'd want it so just that value is negated, but the rest can be displayed
            let dialogMessage = UIAlertController(title: selectedSchool, message: "Number of SAT Test Takers: \(selectedSchoolScores!.numTestTakers!) \nAverage Reading Score: \(selectedSchoolScores!.readingScore!) \nAverage Math Score: \(selectedSchoolScores!.mathScore!) \nAverage Writing Score: \(selectedSchoolScores!.writingScore!)", preferredStyle: .alert)
        
            // Create OK button with action handler
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            })
            //Add OK button to a dialog message
            dialogMessage.addAction(ok)
            // Present Alert to
            self.present(dialogMessage, animated: true, completion: nil)
        }
    }
    
    func captureData(filePath: String) -> [String:SchoolScores]{
        if filePath == nil {
            return schoolInfo
        }
        var csvData: String? = nil
        do {
            //verify contents of file
            csvData = try String(contentsOfFile: filePath, encoding: String.Encoding.utf8)
            let csv = csvData?.csvRows()
            //load each row of contents from the csv file into the dictionary initialized earlier
            for row in csv!{
                //unfortunately this is a more hardcoded way of doing it. I would prefer writing something more dynamic in case the values/columns are changed or if the code is repurposed
                let scores = SchoolScores(numTestTakers: Int(row[2]), readingScore: Int(row[3]), mathScore: Int(row[4]), writingScore: Int(row[5]))
                schoolInfo.updateValue(scores, forKey: row[1])
            }
        } catch{
            print(error)
        }
        
        return schoolInfo
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
